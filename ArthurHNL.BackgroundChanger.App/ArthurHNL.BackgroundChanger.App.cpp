// ArthurHNL.BackgroundChanger.App.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>
#include <filesystem>
#include <Windows.h>

/// <summary>Prints a given message to the stderr stream, then
/// exits with a given exitcode.</summary>
/// <param name='msg'>The message to print to stderr.</param>
/// <param name='exitCode'>The exit code to exit with.</param>
void printErrAndExit(std::string msg, int exitCode)
{
	std::cerr << msg << std::endl;
	std::exit(exitCode);
}

bool fileExists(std::string path)
{
	FILE *fStream;
	errno_t err = fopen_s(&fStream, path.c_str(), "r");
	if (err == 0 && fStream != 0)
	{
		fclose(fStream);
		return true;
	}
	return false;
}

bool setBackground(std::string path)
{
	std::wstring pathWide = std::wstring(path.begin(), path.end());
	const wchar_t* charPath = pathWide.c_str();
	return SystemParametersInfo(
		SPI_SETDESKWALLPAPER,
		0,
		(PVOID) charPath,
		SPIF_UPDATEINIFILE
	);
}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		printErrAndExit("Invalid count of arguments. Usage: BackgroundChanger.exe path_to_image_file.", -1);
	}
	if (!fileExists(argv[1]))
	{
		printErrAndExit("File does not exist.", -2);
	}

	if (!setBackground(argv[1]))
	{
		printErrAndExit("An error occured setting the background.", 1);
	}
	return 0;
}
